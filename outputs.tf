output "vpc_public_subnet_id" {
    value = aws_subnet.public_main.id
}

output "vpc_id" {
    value = aws_vpc.main.id
}